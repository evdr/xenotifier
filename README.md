# XE real time notifications via EMAIL
## Intro
xe.gr automated notifications written in python3 using requests for http connection, beautiful soup for html parsing, sqlite for data persistense and smtplib for sending emails with a gmail account. Whenever a new ad is posted, an email notification will be sent.
This was written 
## How it works
For each search a new process is launched and a new sqlite db file is created.
## How to use
1. Launch a search with desired criteria in xe.gr and copy the auto generated url
2. Create a search config json file in "searches" directory based on the template. Paste the url in the url field and the email account details that will be used for sending emails.
3. For windows:
	- Create a bat script named after your search name according to the example script:
	Update the folowing with your data: (use the correct of python3 command in your system)
	python3 <path_to_directory>\xe_notifier\xelib.py searches/<search_config_json_file>
	- Create a task in Windows Task Scheduler and set the checking frequency.
	- You can conbine many searches in the same bat script by adding another call to "xelib.py" with a diffent search file.
4. For linux:
	- Create a bash script named after your seach name according to example script:
	- Update the folowing with your data: (use the correct of python3 command in your system)
	  python3 <path_to_directory>\xe_notifier\xelib.py searches/<search_config_json_file>
	- Create a cronjob using crontab


## TODO:
- xe_notifier def -ok
- db init if db does not exist -ok
- organize script and db folders, maybe one script per search which is importing xelib with crawler def -ok v1
- notifier function: email solution - ok
- notifier funciton: sms solution
- documentation for creating a new script and adding a cronjob or removing a cronjob
- documentation for setting up rpi o a windows 10 vm running in background.
- test with windows task scheduler - ok

## Licence
MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNE