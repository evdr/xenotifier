# -*- coding: utf-8 -*-
"""
Created on Tue Oct 23 09:21:55 2018

@author: drn

Δομή:
    
    xe_notifier(searchjson)
        getxehtml
        homerent_html_parser
        db_manager
        sms_notifier
        email_notifier
        
Θα τρέχει με cronjob η task scheduler κάθε Χ λεπτά
"""
import requests , sqlite3, json, sys , smtplib, time
from bs4 import BeautifulSoup
from email.message import EmailMessage
from datetime import datetime

def log(msg):
    """ Basic loging function. Logs are written to process.log"""
    text = datetime.strftime(datetime.now(),'%Y-%m-%d %H:%M:%S') + "|" + msg + "\n"
    with open('process.log','a') as f: f.write(text)
    return
def email_notifier(email):
    """Send a simple text email suing gmail service.
    Input: email dict
    Form:
    email = {'Subject': <str>,
             'From'   : <sender email string>,
             'To'     : <recipient email string>,
             'content': <content string>}
    """            
    msg = EmailMessage()
    msg.set_content(email['content'])
    msg['Subject'] = email['Subject']
    msg['From'] = email['From']
    msg['To'] = email['To']
    #Retry for n times if error, if error persists, stop script excecution.
    i=0
    while i<5:
        try:
            server = smtplib.SMTP_SSL("smtp.gmail.com", 465)
            server.ehlo()
            server.login(email['From'], email['pwd'])
            server.send_message(msg)
            server.quit()  
            break   
        except Exception as e:
            log("Email Failed. Retrying in 10s. Exception: {0}".format(str(e)))
            time.sleep(10.0)
            i+=1
    else:
        #stop execution of script
        log("Email Failed - {0} attempts. Exiting...".format(i))
        sys.exit(1)


def getxehtml(search_url ):
    """Get xe.gr html with ads using requests"""

    headers = { "Cache-Control": "max-age=0",
                    "Upgrade-Insecure-Requests": "1",
                    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.67 Safari/537.36",
                    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
                    "Referer": "http://www.xe.gr/property/search?System.item_type=re_residence&Transaction.type_channel=117541&Geo.area_id_new__hierarchy=82371&Transaction.price.to=250&Publication.age=10",
                    "Accept-Encoding": "gzip, deflate",
                    "Accept-Language": "en-US,en;q=0.9,fr;q=0.8,el;q=0.7"}

    #Retry for n times if connection error, if error persists, stop script excecution.
    i=0
    while i<5:
        try:
            r = requests.get(search_url,headers = headers)
            return r.text
            break
        except:
            log("Internet Connection Error. Retrying in 30s")
            time.sleep(2.0)
            i+=1
    else:
        #stop execution of script
        log("Connection error - {0} attempts. Exiting...".format(i))
        sys.exit(1)

def homerent_html_parser(html_obj):
    """Εξαγωγή αγγελίων για ενοικίαση κατοικιών. Δουλεύει σίγουρα για αγγελίες 
    ενοικίασης κατοικιών """
    soup = BeautifulSoup(html_obj,'lxml')
    ad_div = soup.find('div', attrs={'class':'column_468'})
    #Οι αγγελίες απο ιδιώτες έχουν τις κλάσεις'lazy priv r'
    #Οι αγγελίες απο επαγγελματίες έχουν τις κλάσεις 'lazy r'
    #Όλες οι αγγελίες έχουν την κλάση 'lazy' -> έτσι τα βρίσκει όλα
    #Εάν θέλω μόνο απο ιδιώτες, ο σωστός κώδικας είναι: 
    #ads_html = ad_div.find_all('div', attrs={'class':['lazy','priv']})
    ads_html = ad_div.find_all('div', attrs={'class':'lazy'})
    ads = []
    for ad in ads_html:
        ad_obj = {}
        ad_obj['xeid'] = ad['data-id']
        ad_obj['title'] = ad.div.h2.a.text
        ad_obj['creation_date'] = ad['data-edata']
        #οι premium αγγελίες έχουν τις κλάσεις "r_t" και "prem"
        ad_url = ad.find('a', attrs={'class':'r_t'})
        try:
            ad_obj['url'] = 'http://www.xe.gr'+ad_url['href']
        except:
            log("Error with ad url: id = {0}".format(ad['data-id']))
            ad_obj['url'] = 'http://www.xe.gr'
            print(ad_obj['title'])
        price = ad.find('li', attrs={'class':'r_price'})
        try:
            ad_obj['price'] = int(price.text.replace(' €','').strip())
        except:
            ad_obj['price'] = None
        price_li = ad.find('ul',attrs={'class':'r_stats'}).find_all('li')
        re2 = ' τ.μ.'
        re1 = '€ / τ.μ.'
        #Εαν η αγγελία δεν έχει τιμή τότε η λίστα έχει μόνο ένα li με την επιφάνεια
        #Διαφορετικά, έχει 3 li όπου το πρώτο είναι η τιμή, το δεύτερο η επιφάνεια και 
        #το 3ο η τιμή ανα τετραγωνικό
        if ad_obj['price'] == None:
            ad_obj['m2'] = price_li[0].text.replace(re2,'')
            ad_obj['euro_m2'] = None
        else:
            ad_obj['m2'] = price_li[1].text.replace(re2,'')
            ad_obj['euro_m2'] = price_li[2].text.replace(re1,'')            
        
        ads.append(ad_obj)    
    return ads

def db_manager(ads,db_connection):
    """ Βάση δεδομένων. Εαν δεν υπάρχει το αρχείο, τότε δημιουργείται ξανά"""

    cursor = db_connection.cursor()
    #Create table if not exists
    create_table = '''CREATE TABLE IF NOT EXISTS property_rent_ads
                   ( id INTEGER PRIMARY KEY,
                     xeid INTEGER,
                     title TEXT,
                     creation_date TEXT,
                     url TEXT,
                     price INTEGER,
                     m2 TEXT,
                     euro_m2 TEXT,
                     notified INTEGER,
                     notified_date TEXT,
                     capture_date TEXT)''' 
    cursor.execute(create_table)
    db_connection.commit()  
    #For each ad, check if ad exists in db, if not insert, else pass
    new_ads = []
    for ad in ads:     
        sql1= """SELECT * FROM property_rent_ads WHERE xeid ='{0}' AND title='{1}' """.format(ad['xeid'],ad['title'])
        cursor.execute(sql1)    
        res = cursor.fetchall()
        if res:
            pass
        else:
            bs = '?,'
            line = [ ad['xeid'], ad['title'], ad['creation_date'],ad['url'],ad['price'],ad['m2'],ad['euro_m2'],0]
            params = tuple(line)
            sql2 ="INSERT INTO property_rent_ads (xeid,title,creation_date,url,price,m2,euro_m2,notified) VALUES ("+(bs*len(line))[:-1]+")"
            cursor.execute(sql2,params)
            db_connection.commit()
            ad['id'] = cursor.lastrowid
            new_ads.append(ad)
    return new_ads

def sms_notifier(new_ads, config):
    """Send and sms for each new add? """
    #TODO
    #Format sms text
    #Update db entry" notified"    
    return

def ademailcompiler(new_ads,search):
    """Make email notification text"""
    TEXT = u""" """
    for ad in new_ads:
        signle_ad_text = u"""########\n{0}| {1} €\n{2}\n""".format(ad['title'],ad['price'],ad['url'])
        TEXT = TEXT +  signle_ad_text 
    closing = """ URL Αναζήτησης:\n{0}""".format(search['url'])
    TEXT = TEXT + "\n" + closing
    email = {'Subject': u'Αναζήτηση {0}'.format(search['title']),
             'From'   : search["email_sender"],
             'To'     : search['email_recipient'],
             'pwd'    :  search['email_pwd'],
             'content': TEXT }

    return email


def xe_notifier(search):
    """ Integrator function """
    log("Notifier Launched with {0}".format(sys.argv[1]))
    db_connection = sqlite3.connect(search['db_file'])

    #Try to parse html. If error send email. Error will occur if xe.gr changes html.
    try:
        html_obj = getxehtml(search['url'])
    except Exception as e:
        log_msg = "Html parsing failed. Exception: {0}".format(str(e))
        log(log_msg)
        msg = {'Subject': u'Αναζήτηση {0}'.format(search['title']),
                'From'   : search["email_sender"],
                'To'     : search['email_recipient'],
                'pwd'    :  search['email_pwd'],
                'content': log_msg }
        email_notifier(msg)
        sys.exit(1)

    ads = homerent_html_parser(html_obj)
    new_ads = db_manager(ads,db_connection)
    #print(new_ads)
    if len(new_ads) >0:        
        if search["notif_method"] == "sms":
            #sms_notifier(new_ads)
            pass
        elif search["notif_method"] == "email":
            try:
                msg = ademailcompiler(new_ads,search)
                email_notifier(msg)
                log("Email sent with {0} new ads".format(len(new_ads)))
            except Exception as e:
                log("Error while sending email: {0}".format(str(e)))
        else:
            pass
    else:
        log("No new ads")
      

    db_connection.close()
    return

######################################################################    
        
if __name__ == "__main__":
    with open(sys.argv[1]) as f:
        search = json.load(f)
    xe_notifier(search)
